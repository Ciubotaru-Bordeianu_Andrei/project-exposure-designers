﻿Shader "Custom/WaterRipple" {
	Properties {
		_WaveSize("WaveSize", float) = 0.5
		_Speed("Speed", float) = 0.5
		_Frequency("Frequency", float) = 0.5

		_SideSpeed("SideSpeed", float) = 1
		_Amplitude("Amplitude", float) = 1
		_Distance("Distance", float) = 1
		_Amount("Amount", float) = 1
		_Position("Position",vector) = (0,0,0)

		_TintColor("Tint Color", Color) = (1,1,1,1)
		_Transparency("Transparency", Range(0.0,0.5)) = 0.25
		_CutoutThresh("Cutout Threshold", Range(0.0,1.0)) = 0.2

		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
			Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200

			//ZWrite Off
			//Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
		};

		half _WaveSize,_Speed, _Frequency;
		half _SideSpeed, _Amplitude, _Distance, _Amount;

		void vert (inout appdata_full v){
			half yoffset = (v.vertex.x *  v.vertex.x) + (v.vertex.z *  v.vertex.z);
			half val = _WaveSize * sin(_Time.w * _Speed + yoffset * _Frequency);


			v.vertex.y += val;
			//v.vertex.x += offsetx;
		}



		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		half _TintColor;
		half _Transparency;
		half _Position;
		half _CutoutThresh;
		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			//float3 direction = IN.worldPos-_Position;
			//float2 dir = direction.xz;
			float2 dir = float2(IN.uv_MainTex.x - 0.5, IN.uv_MainTex.y - 0.5);
			float x = (dir.x ) *sin(_Time.y);
			float y = (dir.y ) *sin(_Time.y);
			//float2 offset = (sin(_SideSpeed + (IN.uv_MainTex.x *x)* _Amplitude) * _Distance * _Amount, sin(_SideSpeed + (IN.uv_MainTex.y * y)* _Amplitude) * _Distance * _Amount);
			float2 offset = (x,y)*_Amplitude;
			//half xoffset = sin(_Time.y * _Speed + IN.uv_MainTex.x * _Amplitude) * _Distance * _Amount;
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex + offset) * _Color;
			o.Albedo = c.rgb;
			//// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic;
			//o.Smoothness = _Glossiness;
			//o.Alpha = _Transparency;
			/*if (sqrt(x*x + y * y) < 0.5f)
			{
				o.Albedo = (0.5, 0.5, 0);
			}*/
		}
		ENDCG
	}
	FallBack "Diffuse"
}
