﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/WaterTest"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		_NormalTex("Normal", 2D) = "normal"{}

		_WaveSize("WaveSize", float) = 0.5
		_Speed("Speed", float) = 0.5
		_Frequency("Frequency", float) = 0.5

		_Amplitude("Amplitude", float) = 1
		_Distance("Distance", float) = 1
		_Position("Position",vector) = (0,0,0)


		_Color("Color", Color) = (1,1,1,1)

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			half _WaveSize,_Speed, _Frequency;
			half _Amplitude, _Distance;

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;

			};

			uniform sampler2D _MainTex;
			uniform sampler2D _NormalTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;

				half yoffset = (v.vertex.x *  v.vertex.x) + (v.vertex.z *  v.vertex.z);
				half val = _WaveSize * sin(_Time.w * _Speed + yoffset * _Frequency);

				v.vertex.y += val;

				o.vertex = UnityObjectToClipPos(v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv = v.uv;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			fixed4 _Color;
			fixed4 _Position;
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 dir = float2(1,1) ;//float2(i.uv.x - _Position.x, i.uv.y - _Position.y);
				//float size = sqrt(dir.x*dir.x + dir.y*dir.y);
				//float size = sqrt(i.uv.x*i.uv.x + i.uv.y*i.uv.y);

				/*if (size < 0.2)
				{
					return _Color;
				}*/
				dir = normalize(dir)*/*abs*/(/*sin*/(_Time.w*_Distance)*2)*_Amplitude;
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv+dir) * _Color;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
